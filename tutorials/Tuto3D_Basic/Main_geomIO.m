% ==========================================
% Basic 3D example
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

clc, clear, close all
opt                 = geomIO_Options();
opt.DrawCoordRes    = 10;
opt.inputFileName   = ['Input/RabbitAndCat.HZ.svg'];
opt.writeParaview   = true;
opt.shiftPVobj      = [0 0 1];

opt.readLaMEM           = true;
opt.NewLaMEM            = false;
opt.LaMEMinputFileName  = './Input/RabbitAndCat.dat';
opt.writePolygons       = true;
[~, Volumes]            = run_geomIO(opt);

plotVolumes(Volumes,{'CatAndRabbit'})
