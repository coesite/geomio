function dgz = gravSphere(R,drho,r,b)
%
% dgz = gravSphere(R,drho,r,b)
% input must be in SI units
% returns dgz in mGal
% ====================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   gravSphere.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================


disp ('Assumption: coordinates are in meters')
disp ('Assumption: b positive with depth')

% constants
gamma   = 6.67*1e-11;
si2mGal = 1e5;

% Vertical gravity acceleration
dgz = 4/3*pi*gamma*drho*R^3 *  b./(r.^2+b.^2).^1.5 * si2mGal;

end
