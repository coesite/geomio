% Display the path to include into your startup.m to make geomIO toolbox
% functions visible within the MATLAB environment
clc
cd src
fprintf('To make the geomIO toolbox available on your system, add ... \n\n  addpath(genpath(''%s'')); \n\n...to your startup.m file.\n\n',pwd);

if isempty(which('startup.m'))   
    if isempty(userpath)
        fprintf('Your userpath seems not to be defined. Check out the MATLAB help.\n');
        fprintf('We recommend to reset the user path with: userpath(''reset'')\n\n')
        
        help userpath
    else
        fprintf('Add an empty startup.m file in %s and modify it as indicated above.\n',userpath);
    end    
else
    fprintf('The startup.m file is located in %s.\n',userpath);   
end

cd ..


