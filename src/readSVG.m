function [PathCoord,docNode,opt,svg] = readSVG(opt)
%
% PathCoord = readSVG(opt)
% read paths from SVG file with multiple layers
%
% Plot paths:
% opt.debug = true;
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   readSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% Create DOM elements
tic
docNode = xmlread(opt.inputFileName);
fprintf('xml parsing: %.1fs\n',toc)
% Get svg attributes
svgList     = docNode.getElementsByTagName('svg');
currentSVG  = svgList.item(0);
viewBox     = char(currentSVG.getAttribute('viewBox'));

%if isempty(viewBox)
if 1 % i think we should always enter this (Arne)
    opt.svg.height  = (currentSVG.getAttribute('height'));
    opt.svg.width   = (currentSVG.getAttribute('width'));
    
    % Legacy stuff, note that if mm or px are the units, there might be
    % problems
    opt.svg.height = regexprep(char(opt.svg.height),'m+','');
    opt.svg.width  = regexprep(char(opt.svg.width),'m+','');
    
    opt.svg.height = regexprep(char(opt.svg.height),'px','');
    opt.svg.width  = regexprep(char(opt.svg.width),'px','');
    
    opt.svg.height  = str2double(opt.svg.height);
    opt.svg.width   = str2double(opt.svg.width);
    
    
else
    viewBox = str2num((viewBox));
    opt.svg.width = viewBox(3);
    opt.svg.height = viewBox(4);
end

ReadZFromFile = strcmp(opt.z,'ReadZFromFile');
if ReadZFromFile
    opt.z = [];
end


if isnan(opt.svg.height) || isnan(opt.svg.width)
    error(' svg height or width are NaN. The unit of your svg document must be in pixels');
end

% Get the list of g nodes (groups)
%  NOTE: with 'Graphic' this is problematic, as it may also contain sub-groups (groups within groups)
%   
layerList = docNode.getElementsByTagName('g');
noLayers = layerList.getLength;

switch opt.svgEditor
    case 'Graphic'
       % in Autodesk GRAPHIC, also individual polygons are listed as g (groups) object, but they are part of a layer
       % just asking byTagName will list both layers 
       
       LayerInd = [];
       for il = 1:noLayers
            if strcmp(layerList.item(il-1).getParentNode.getNodeName,'svg')
                LayerInd = [LayerInd; il-1];
            end
       end
       noLayers=length(LayerInd);
       
end



if isempty(opt.pathNames)
    FillPathNames = true;
else
    FillPathNames = false;
end

if isempty(opt.phase)
    FillPhase = true;
else
    FillPhase = false;
end


% Read paths
% Initialize variables
PathCoord = struct();
C = 0;
visitedRefLayer = false;
firstone = true;
toberead = true;

%% Get info from Reference layer
for il = 1:noLayers
    
    % Get the current g node (layer)
    switch opt.svgEditor
        case 'Graphic'
            currentLayer = layerList.item(LayerInd(il));
        otherwise
            currentLayer = layerList.item(il-1);
    end
    LayerTransformationString    = char(currentLayer.getAttribute('transform'));
    
    switch opt.svgEditor
        case 'Inkscape'
            layerLabel   = char(currentLayer.getAttribute('inkscape:label'));
        case 'Illustrator'
            layerLabel   = char(currentLayer.getAttribute('id'));
        case 'Graphic'
            layerLabel   = char(currentLayer.getAttribute('id'));
        otherwise
            error('Unknown SVG editor')
    end
    if ~isempty(layerLabel)
        if regexp(layerLabel(1),'\d') % if the first character is a digit assume it refers to a positive number and add p in front of the layerLabel
            layerLabel = ['p' layerLabel];  % structure field names cannot start by a number, therefore a letter is added
        end
    end
    if strcmp(layerLabel,'Reference')
        %% =================================================
        %                   Reference layer
        % ==================================================
        RefPath     = currentLayer.getElementsByTagName('path').item(0);
        if ~isempty(RefPath)
            PathData    = char(RefPath.getAttribute('d'));
            [InputCoord, ~, ~]  = readPath(opt, PathData);
        else
            RefPath     = currentLayer.getElementsByTagName('line').item(0);
            x1 = str2num(char(RefPath.getAttribute('x1')));
            y1 = str2num(char(RefPath.getAttribute('y1')));
            x2 = str2num(char(RefPath.getAttribute('x2')));
            y2 = str2num(char(RefPath.getAttribute('y2')));
            InputCoord = [x1 x2 ; y1 y2];
        end
        
        opt.xrefPaper(1)    = InputCoord(1,1);
        opt.yrefPaper(1)    = InputCoord(2,1);
        opt.xrefPaper(2)    = InputCoord(1,2);
        opt.yrefPaper(2)    = InputCoord(2,2);
        
        
%                   plot([opt.xrefPaper(1) opt.xrefPaper(2) opt.xrefPaper(2) opt.xrefPaper(1) opt.xrefPaper(1)],...
%                  [opt.yrefPaper(1) opt.yrefPaper(1) opt.yrefPaper(2) opt.yrefPaper(2) opt.yrefPaper(1)],'ro-')
%         hold on
        
        opt.yrefPaper       = opt.svg.height - opt.yrefPaper;
        switch opt.svgEditor
            case 'Inkscape'
                CoordRef = char(RefPath.getAttribute('CoordRef'));
            case 'Illustrator'
                CoordRef = char(RefPath.getAttribute('id'));
                CoordRef = CoordRef(9:end);
                CoordRef = regexprep(CoordRef,'_',' ');
           
            case 'Graphic'
                CoordRef = char(RefPath.getAttribute('id'));
                CoordRef = CoordRef(9:end);
                CoordRef = regexprep(CoordRef,'_',' ');
                CoordRef = regexprep(CoordRef,',',' ');
                
            otherwise
                error('Unknown SVG editor')
        
        end
        
        CoordRef            = str2num(CoordRef);
        opt.xref(1)         = CoordRef(1);
        opt.yref(1)         = CoordRef(2);
        opt.xref(2)         = CoordRef(3);
        opt.yref(2)         = CoordRef(4);
        fprintf('> Found Reference layer\n')
        fprintf('> xref: [%g %g], yref: [%g %g]\n\n',opt.xref(1), opt.xref(2),opt.yref(1), opt.yref(2) );
        visitedRefLayer = true;
    end
    
end



for il = 1:noLayers
    
    % Get the current g node (layer)
    switch opt.svgEditor
        case 'Graphic'
             currentLayer = layerList.item(LayerInd(il));
        otherwise
            currentLayer = layerList.item(il-1);
    end
    LayerTransformationString    = char(currentLayer.getAttribute('transform'));

    
    switch opt.svgEditor
        case 'Inkscape'
            layerLabel   = char(currentLayer.getAttribute('inkscape:label'));
        case 'Illustrator'
            layerLabel   = char(currentLayer.getAttribute('id'));
        case 'Graphic'
            layerLabel   = char(currentLayer.getAttribute('id'));
            layerLabel_MAIN = layerLabel;
        otherwise
            error('Unknown SVG editor')
    end
    
    if ~isempty(layerLabel)
        if regexp(layerLabel(1),'\d') % if the first character is a digit assume it refers to a positive number and add p in front of the layerLabel
            layerLabel = ['p' layerLabel];  % structure field names cannot start by a number, therefore a letter is added
        end
    end
    
    
    %% Layer switches
    %     currentLayer.setIdAttribute('id', true);
    if isempty(layerLabel)
        disp(['> Found empty layer #' num2str(il)]);
        disp(['>> Might be due to groups: Please ungroup paths!']);
    elseif strcmp(layerLabel(1),'#')
        disp(['> Ignore ' layerLabel]);
    elseif strcmp(layerLabel,'Reference')
        % Note: Reference is read before, to ensure that all layers are
        % scaled properly
        
    elseif strcmp(layerLabel,'SlabAgeFunction')
        % Note: Work in progress
        
    elseif strcmp(layerLabel,'Background')
        disp(['> Ignore ' layerLabel]);
    elseif strcmp(layerLabel,'Box')
        fprintf('> Found Box layer\n')
        Box     = currentLayer.getElementsByTagName('rect').item(0);
        x = str2num(char(Box.getAttribute('x')));
        y = str2num(char(Box.getAttribute('y')));
        width = str2num(char(Box.getAttribute('width')));
        height = str2num(char(Box.getAttribute('height')));
        Coord = [x x+width; y y+height];
        Coord(2,:)  = opt.svg.height - Coord(2,:);
        
        Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
        Coord(2,:) = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
        fprintf('> Coordinates of the Box: \n     lower left corner: %f, %f \n     upper right corner: %f, %f\n',min(Coord(1,:)), min(Coord(2,:)), max(Coord(1,:)), max(Coord(2,:)));
        opt.Box.xmin = min(Coord(1,:));
        opt.Box.ymin = min(Coord(2,:));
        opt.Box.xmax = max(Coord(1,:));
        opt.Box.ymax = max(Coord(2,:));
        
        if ~visitedRefLayer
            fprintf('> Reference layer not visited (yet?). Coordinates are given in the drawing reference. If a Reference layer is defined, please update the order of layers so that Box is above Reference\n')
        end
        fprintf('\n')
    elseif strcmp(layerLabel,'COMpath')
        fprintf('> Found COMpath layer\n')
        
        PathList     = currentLayer.getElementsByTagName('path');
        noPaths = PathList.getLength();
        if noPaths == 0
            fprintf('> No Center of mass path was found was found.\n')
        else
            fprintf('> The following center of mass path were found:\n')
        end
        
        for ip = 1:noPaths
            currentPath     = PathList.item(ip-1);
            [pathLabel, InputCoord, ~, ~, ~, ~, ~] = getPathData(currentPath, 'path', LayerTransformationString, opt);
%             InputCoord(2,:)  = opt.svg.height - InputCoord(2,:);
            InputCoord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,InputCoord(1,:));
            InputCoord(2,:) = linCoordTrans(opt.yrefPaper,opt.yref,InputCoord(2,:));
            InputCoordStr = InputCoord;
            InputCoordStr = num2str(InputCoordStr(:)');
            fprintf(['  %s: ' InputCoordStr  '\n'], pathLabel)
        end
        
        if ~visitedRefLayer
            fprintf('> Reference layer not visited (yet?). Coordinates are given in the drawing reference. If a Reference layer is defined, please update the order of layers so that Box is above Reference\n')
        end
        fprintf('\n')
    elseif strcmp(layerLabel,'PushingBoxes')
        fprintf('\n> Found PushingBoxes layer\n')
        
        BoxList     = currentLayer.getElementsByTagName('rect');
        noBoxes = BoxList.getLength();
        
        if noBoxes == 0
            fprintf('> No pushing box was found.\n')
        else
            fprintf('> The following pushing boxes were found:\n')
        end
        
        for iB = 1:noBoxes
            Box     = BoxList.item(iB-1);
            x = str2num(char(Box.getAttribute('x')));
            y = str2num(char(Box.getAttribute('y')));
            width = str2num(char(Box.getAttribute('width')));
            height = str2num(char(Box.getAttribute('height')));

            Coord = [x x+width; y y+width];
            Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
            Coord(2,:) = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
            
            xc = (Coord(1,1) + Coord(1,2))/2;
            yc = (Coord(2,1) + Coord(2,2))/2;
            
            [pathLabel, ~, ~, ~, ~, ~, ~] = getPathData(Box, 'rect', LayerTransformationString, opt);
            
            fprintf('  %s: xc %.1f yc %.1f width %.1f height %.1f\n',pathLabel, xc, yc, width, height);
        end
        
        %% Code fragment for the general box. Not supported yet
%         
%         ChildList  = currentLayer.getChildNodes;
%         noChildren      = ChildList.getLength;
%         ChildNumber = [];
%         noPaths = 0;
%         for iC = 1:noChildren
%             thisChildName = char(ChildList.item(iC-1).getNodeName);
%             switch thisChildName
%                 case {'path','rect','ellipse'}
%                     ChildNumber(end+1) = iC-1;
%                     noPaths = noPaths + 1;
%                 case '#text'
%                     
%                 otherwise
%                     warning(sprintf('found unknwon tag: %s in layer %s', thisChildName,layerLabel));
%             end
%         end
%         
%         if noPaths == 0
%             fprintf('> No pushing box was found.\n')
%         else
%             fprintf('> The following pushing boxes were found:\n')
%         end
%         
%         for ip = 1:noPaths
%             [pathLabel, InputCoord, DrawCoord, Commands, PathStyle, StartSubPath, Phase] = getPathData(currentPath, opt)
%         end
        
        fprintf('> Rotation of pushing boxes are not given in the current version of geomIO\n')
        
        
        
        if ~visitedRefLayer
            fprintf('> Reference layer not visited (yet?). Coordinates are given in the drawing reference. If a Reference layer is defined, please update the order of layers so that Box is above Reference\n')
        end
        fprintf('\n')
    elseif strcmp(layerLabel,'Profiles')
        
    elseif strcmp(layerLabel,'Grid')
        
        
    else
        %% =================================================
        %                   Drawing layer
        % ==================================================
        % Get background image
        if toberead
            
            imageList = currentLayer.getElementsByTagName('image');
            noImages  = imageList.getLength;
            
            if noImages == 1
                warning('geomIO: We recommend to use images only on layers with a # name');
                thisimage = imageList.item(0);
                opt.imgWidth  = str2double(char(thisimage.getAttribute('width')));
                opt.imgHeight = str2double(char(thisimage.getAttribute('height')));
                opt.imgX      = str2double(char(thisimage.getAttribute('x')));
                opt.imgY      = str2double(char(thisimage.getAttribute('y')));
                
                opt.imgDir    = char(thisimage.getAttribute('xlink:href'));
                str_idx       = strfind(opt.imgDir,'.');
                opt.imgType   = opt.imgDir(str_idx(end)+1:end);
                str_idx       = strfind(opt.imgDir,'/');
                opt.imgDir    = opt.imgDir(1:str_idx(end));
            elseif noImages > 1
                error(['You have more than one image on layer ' layerLabel]);
            else
                % no images found
            end
            
            toberead= false;
        end
        
        %% =================================================
        %                   Go through paths
        % ==================================================
        
        noPaths = 0;
        switch opt.svgEditor
            case 'Graphic'
                % NOTE:
                %   if it is a filled contour, Graphic saves them as a group
                %   (one with the contour and one with the path), which is
                %   one level deeper
                %   
                %   If it is not a filled contour they are simply listed as
                %   a path on this particular level
                
%                 ChildList   =   currentLayer.getElementsByTagName('g');
%                 noChildren  =   ChildList.getLength;
%                 noPaths      =   noChildren;
                
                ChildList   =   currentLayer.getChildNodes;
                noChildren  =   ChildList.getLength;
                
            otherwise
                ChildList   =   currentLayer.getChildNodes;
                noChildren  =   ChildList.getLength;
        end
        
        
        
        ChildNumber = [];

        for iC = 1:noChildren
            
            switch opt.svgEditor
                case 'Graphic'
                      thisChildName = char(ChildList.item(iC-1).getNodeName);
                      switch thisChildName
                        case {'path','rect','ellipse'}
                            ChildNumber(end+1) = iC-1;
                            noPaths = noPaths + 1;
                        case '#text'
                          
                        case 'g'
                            % it is likely a filled contour
                            
                            % do some checking here to esnure that this is
                            % the case
                            ChildNumber(end+1) = iC-1;
                            noPaths = noPaths + 1;
                            
                        otherwise
                            warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
                      end
                    
                      
%                     % in Graphic, 
%                     
%                     ChildChildList = ChildList.item(iC-1);
%                     num = ChildChildList.getLength;
%                     for iNum=1:num
%                         thisChildName = char(ChildChildList.item(iNum-1).getNodeName);
%                         switch thisChildName
%                             case {'path','rect','ellipse'}
%                                 ChildNumber(end+1) = iC-1;
%                                 noPaths = noPaths + 1;
%                             case '#text'
%                                 
%                             otherwise
%                                 warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
%                         end
%                     end
                    
                otherwise
                    thisChildName = char(ChildList.item(iC-1).getNodeName);
                    
                    
                    switch thisChildName
                        case {'path','rect','ellipse'}
                            ChildNumber(end+1) = iC-1;
                            noPaths = noPaths + 1;
                        case '#text'
                            
                        otherwise
                            warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
                    end
            end
            
            
        end
        
        for ip = 1:noPaths
            switch opt.svgEditor
                case 'Graphic'
                     thisChildName = char(ChildList.item(ChildNumber(ip)).getNodeName);
                      switch thisChildName
                        case {'path','rect','ellipse'}
                            currentPath = ChildList.item(ChildNumber(ip));
                            PathType    = char(currentPath.getNodeName);
                            ChildLevel  = 0;
                        case '#text'
                          
                        case 'g'
                            % it is likely a filled contour - filled
                            % contours in Graphic consist of two path's:
                            % one with color of the fill-in, and one with
                            % the line color
                            ChildChildList = ChildList.item(ChildNumber(ip));
                            
                            num = ChildChildList.getLength;
                            for iNum=1:num
                                thisChildName = char(ChildChildList.item(iNum-1).getNodeName);
                                switch thisChildName
                                    case {'path','rect','ellipse'}
                                        
                                        currentPath = ChildChildList.item(iNum-1);
                                        PathType    = char(currentPath.getNodeName);
                                        ChildLevel  = 1;    % one level down
                                        break
                                    case '#text'
                                        
                                    otherwise
                                        warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
                                end
                            end
                            
                        otherwise
                            warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
                      end
                      
                    
%                     ChildChildList = ChildList.item(ip-1);
%                     num = ChildChildList.getLength;
%                     for iNum=1:num
%                         thisChildName = char(ChildChildList.item(iNum-1).getNodeName);
%                         switch thisChildName
%                             case {'path','rect','ellipse'}
%                                 
%                                 currentPath = ChildChildList.item(iNum-1);
%                                 PathType    = char(currentPath.getNodeName);
%                                 break
%                             case '#text'
%                                 
%                             otherwise
%                                 warning(sprintf('found unknown tag: %s in layer %s', thisChildName,layerLabel));
%                         end
%                     end

                otherwise
                    % Get path attributes
                    currentPath = ChildList.item(ChildNumber(ip));
                    PathType    = char(currentPath.getNodeName);
                    ChildLevel  = 0;
            end
            
            [pathLabel, InputCoord, DrawCoord, Commands, PathStyle, StartSubPath, Phase,StyleString] = getPathData(currentPath, PathType, LayerTransformationString, opt, ChildLevel);
            
            if isempty(pathLabel)
                pathLabel = ['path_',num2str(ip)];
            end

            
            if opt.debug
                figure(il)
                % Draw
                hold on
                %                 axis equal
                axis([0 opt.svg.width 0 opt.svg.height])
                switch PathType
                    case 'path'
                        drawPath( PathStyle, InputCoord, DrawCoord, Commands,  'debug' );
                    case {'rect','ellipse'}
                        plot(DrawCoord(1,:),DrawCoord(2,:) ,'-k','Color',PathStyle.stroke./255,'Linewidth',PathStyle.stroke_width+0.0001)
                end
                
            end
           
            % Fill the PathCoord struct
            PathCoord.(layerLabel).(pathLabel).InputCoord = InputCoord;
            PathCoord.(layerLabel).(pathLabel).DrawCoord  = DrawCoord;
            PathCoord.(layerLabel).(pathLabel).Commands   = Commands;
            PathCoord.(layerLabel).(pathLabel).PathStyle  = PathStyle;
            PathCoord.(layerLabel).(pathLabel).StartSubPath= StartSubPath;
            PathCoord.(layerLabel).(pathLabel).Phase       = Phase;
            PathCoord.(layerLabel).(pathLabel).StyleString = StyleString;
            
            if FillPathNames
                C = C+1;
                opt.pathNames{C} = pathLabel;
                switch opt.svgEditor
                    case 'Inkscape'
                        opt.phase(C) = Phase;
                    case 'Illustrator'
                        % No support yet for writing phase directly in the
                        % document in Illustrator
                    case 'Graphic'
                        % No support yet for writing phase directly in the
                        % document in Graphic
                    otherwise
                        error('Unknown SVG editor')
                end
            elseif isempty(opt.phase)
                I = find(strcmp(opt.pathNames,pathLabel));
                opt.phase(I) = Phase;
            end
            
            
            
        end

        if ReadZFromFile
            if ~isempty(regexp(layerLabel,'m\d+', 'once')) && (noPaths ~=0)
                % decimal number?               
                frmstr = regexp(layerLabel,'[0-9]p[0-9]');                
                nstart = regexp(layerLabel,'m\d+', 'once');                
                if isempty(frmstr)
                    num = str2double(layerLabel(regexp(layerLabel,'[0-9]')));
                else
                    num = str2double(strrep(layerLabel(nstart+1:end),'p','.'));
                end
                opt.z = [opt.z -num];
            elseif ~isempty(regexp(layerLabel,'[p]?\d+', 'once')) && (noPaths ~=0)
                % decimal number?               
                frmstr = regexp(layerLabel,'[0-9]p[0-9]');                
                nstart = regexp(layerLabel,'[p]?\d+', 'once');                
                if isempty(frmstr)
                    num = str2double(layerLabel(regexp(layerLabel,'[0-9]')));
                else
                    num = str2double(strrep(layerLabel(nstart+1:end),'p','.'));
                end
                opt.z = [opt.z num];               
            end
        end
        
    end
end

if strcmp(opt.zi,'NotSpecified')
    opt.zi = opt.z;
end


% Control message if reference layer was read
if ~visitedRefLayer
    disp('> No reference layer found, assigning xrefPaper to xref, yrefPaper to yref');
    opt.xrefPaper(1)    = opt.xrefPaper(1);
    opt.yrefPaper(1)    = opt.yrefPaper(1);
    opt.xrefPaper(2)    = opt.xrefPaper(2);
    opt.yrefPaper(2)    = opt.yrefPaper(2);
    opt.yrefPaper       = opt.svg.height - opt.yrefPaper;

    opt.xref(1)         = opt.xrefPaper(1);
    opt.yref(1)         = opt.yrefPaper(1);
    opt.xref(2)         = opt.xrefPaper(2);
    opt.yref(2)         = opt.yrefPaper(2);
end

% --- update opt structure ---

% save path names
if FillPathNames
    [opt.pathNames,I,~] = unique(opt.pathNames);
    if ~isempty(opt.phase)
        opt.phase = opt.phase(I);
    end
end

% Test for discrepancy between the opt structure and the file
% do opt.pathNames exist?
for iP = 1:length(opt.pathNames)
    [ layerFields ] = findPathsInLayers( PathCoord, opt.pathNames{iP} );
    if isempty(layerFields)
        error('the pathName "%s" does not exist in the svg file. Please change opt.pathNames', opt.pathNames{iP})
    end
end

fprintf('\n');
end

function OptionValue = extractStyle(String, optName)
OptionValue = regexp(String,[optName ':[^;]+'],'match');
%     Option = regexp(StyleString,'fill:[^;]+','match')
if ~isempty(OptionValue)
    OptionValue = OptionValue{1}(length(optName)+2:end);
else
    OptionValue = 'empty';
end
end

function [pathLabel, InputCoord, DrawCoord, Commands, PathStyle, StartSubPath, Phase,StyleString] = getPathData(currentPath, PathType, LayerTransformationString, opt, ChildLevel)

switch opt.svgEditor
    case 'Inkscape'
        pathLabel = char(currentPath.getAttribute('label'));
        if isempty(pathLabel)
            pathLabel = char(currentPath.getAttribute('id'));
        end
    case 'Illustrator'
        pathLabel = char(currentPath.getAttribute('id'));
        I = regexp(pathLabel,'_\d+_');
        if ~isempty(I)
            pathLabel = pathLabel(1:I-1);
        end
        
    case 'Graphic'
        switch ChildLevel
            case 1
                % in case we have a closed contour
                pathLabel = char(currentPath.getParentNode.getAttribute('id'));
            case 0
                pathLabel = char(currentPath.getAttribute('id'));
                 
        end
        
        
      
        
    otherwise
        error('Unknown SVG editor')
end

% Extract data

% Extract style
switch opt.svgEditor
    case 'Inkscape'
        StyleString   = char(currentPath.getAttribute('style'));
        PathStyle.fill      = extractStyle(StyleString, 'fill');
        PathStyle.fill_opacity      = extractStyle(StyleString, 'fill-opacity');
        if strcmp(PathStyle.fill_opacity,'empty')
            PathStyle.fill_opacity      = 1;
        else
            PathStyle.fill_opacity      = str2double(PathStyle.fill_opacity);
        end
        PathStyle.stroke    = extractStyle(StyleString, 'stroke');
        PathStyle.stroke_width    = extractStyle(StyleString, 'stroke-width');
        
        
        
        Phase = char(currentPath.getAttribute('phase'));
        if isempty(Phase)
            Phase = 0;
        else
            Phase = str2double(Phase);
        end
        
    case 'Illustrator'
        PathStyle.fill              =   char(currentPath.getAttribute('fill'));
        PathStyle.stroke            =   char(currentPath.getAttribute('stroke'));
        % fill-opacity untested for illustrator
        PathStyle.fill_opacity      =   char(currentPath.getAttribute('fill-opacity'));
        PathStyle.stroke_width      =   char(currentPath.getAttribute('stroke-width'));
        Phase                       =   0;
        StyleString                 =   [];
         
    case 'Graphic'
        PathStyle.fill              =   char(currentPath.getAttribute('fill'));
        if isempty(PathStyle.fill)
            PathStyle.fill='none';
        end
            
        PathStyle.stroke            =   char(currentPath.getAttribute('stroke'));
        if isempty(PathStyle.stroke)
            PathStyle.stroke        =   'none';
        end
        
        % fill-opacity untested for illustrator
        PathStyle.fill_opacity      =   char(currentPath.getAttribute('fill-opacity'));
        if isempty(PathStyle.fill_opacity)
            PathStyle.fill_opacity  =   1;
        end
        
        PathStyle.stroke_width      =   char(currentPath.getAttribute('stroke-width'));
        if isempty(PathStyle.stroke_width)
            PathStyle.stroke_width  =   'none';
        end
        
        Phase                       =   0;
        StyleString                 =   [];

    otherwise
        error('Unknown SVG editor')
        
end
if ~strcmp(PathStyle.stroke,'none') && ~strcmp(PathStyle.stroke,'empty') && ~strcmp(PathStyle.stroke,'')
    PathStyle.stroke       = hex2rgb(PathStyle.stroke(2:end))./255;
end
if ~isempty(PathStyle.stroke_width)
    PathStyle.stroke_width = regexp(PathStyle.stroke_width,'[^px]','match');
    PathStyle.stroke_width = str2double(PathStyle.stroke_width{1});
end

if ~strcmp(PathStyle.fill,'none')  && ~strcmp(PathStyle.fill,'empty') && ~strcmp(PathStyle.fill,'')
    PathStyle.fill = hex2rgb(PathStyle.fill(2:end))./255;
end


% Read path and extract coordinates
switch PathType
    case 'path'
        % Get PathData
        PathData    = char(currentPath.getAttribute('d'));
        % Get InputCoord
        [InputCoord, DrawCoord, Commands, StartSubPath] = readPath(opt, PathData);
        % Apply eventual transformations
        TransformationString    = char(currentPath.getAttribute('transform'));
        [DrawCoord] = svgTransform(TransformationString,DrawCoord);
        [DrawCoord] = svgTransform(LayerTransformationString,DrawCoord);
        [InputCoord] = svgTransform(TransformationString,InputCoord);
        [InputCoord] = svgTransform(LayerTransformationString,InputCoord);
        % Get DrawCoord
        
%           plot([opt.xrefPaper(1) opt.xrefPaper(2) opt.xrefPaper(2) opt.xrefPaper(1) opt.xrefPaper(1)],...
%                  [opt.yrefPaper(1) opt.yrefPaper(1) opt.yrefPaper(2) opt.yrefPaper(2) opt.yrefPaper(1)],'o-')
        
        %                     [InputCoord, DrawCoord, Commands] = readPath(opt, InputCoord, Commands);
        InputCoord(2,:) = opt.svg.height - InputCoord(2,:);
        
    case 'rect'
        x = str2num(char(currentPath.getAttribute('x')));
        y = str2num(char(currentPath.getAttribute('y')));
        width = str2num(char(currentPath.getAttribute('width')));
        height = str2num(char(currentPath.getAttribute('height')));
        
        DrawCoord = [x y               ;...
            x+width y         ;...
            x+width y+height  ;...
            x       y+height
            x y]';
        %                     InputCoord = [x y ; width height];
        
        % Apply eventual transformations
        TransformationString    = char(currentPath.getAttribute('transform'));
        [DrawCoord] = svgTransform(TransformationString,DrawCoord);
        [DrawCoord] = svgTransform(LayerTransformationString,DrawCoord);
        
        InputCoord = DrawCoord;
        % This make things easy as the write SVG function doesn't
        % have to be modified. However it is not so nice since
        % in the new svg file rect, ellipse etc... will become
        % path (sucks especially for ellipses)
        
        
        InputCoord(2,:) = opt.svg.height - InputCoord(2,:);
        Commands.SectionLength  = size(InputCoord,2);
        Commands.String         = 'M';
        StartSubPath = [1 length(DrawCoord)+1];
    case 'ellipse'
        cx = str2num(char(currentPath.getAttribute('cx')));
        cy = str2num(char(currentPath.getAttribute('cy')));
        rx = str2num(char(currentPath.getAttribute('rx')));
        ry = str2num(char(currentPath.getAttribute('ry')));
        
        
        phi         = linspace(0,2*pi,opt.DrawCoordRes*4);
        DrawCoord   = [cx+rx*cos(phi) ; cy+ry*sin(phi)];
        
        % Apply eventual transformations
        TransformationString    = char(currentPath.getAttribute('transform'));
        [DrawCoord] = svgTransform(TransformationString,DrawCoord);
        [DrawCoord] = svgTransform(LayerTransformationString,DrawCoord);
        %                     InputCoord  = [cx cy ; rx ry];
        InputCoord = DrawCoord;
        InputCoord(2,:) = opt.svg.height - InputCoord(2,:);
        Commands.SectionLength  = size(InputCoord,2);
        Commands.String         = 'M';
        
        str2num(char(currentPath.getAttribute('transform')));
        StartSubPath = [1 length(DrawCoord)+1];
end


DrawCoord(2,:)  = opt.svg.height - DrawCoord(2,:);


end

