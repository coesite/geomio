function dgz =polygrav3D(polygons,opt,mode)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   geomIO_Options.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% polygrav3D computes the 3D gravity anomaly at P=(0,0,0) 
% implemented after Talwani, M. & Ewing, M. 1960
% Tobias S Baumann 2018 Mainz
% =========================================================================

%disp ('Assumption: z should be positive with depth -> Scale z coordinates with -1 !!')

if strcmp(mode,'PathCoord')
    disp ('Assumption: must be HZ.SVG !!')
    PathCoord = polygons;
elseif strcmp(mode,'Volumes')
    Volumes = polygons;
else
    error('This option does not exist. Try >PathCoord< or >Volumes<')
end

% constants
gamma   = 6.67*1e-11;
si2mGal = 1e5;
switch opt.gravity.lenUnit
    case 'km'
        km2m=1e3;
    case 'm'
        km2m=1;
end


switch mode
   
    case 'PathCoord'
        % dz
        dz = diff(opt.zi)*km2m;
        dz_lamina = [dz(1)/2 dz(1:end-1)/2+dz(2:end)/2 dz(end)/2];

        % lamina
        layers = fieldnames(PathCoord);

        % cell array to store anomalies
        dgz   = cell(1,length(opt.pathNames)+1);
        count = 1;

        % loop over pathnames
        for kvol = 1:length(opt.pathNames)           
            % current object
            pathName = opt.pathNames{kvol};
            % initialize survey
            dgzl = zeros(size(opt.gravity.survey_x));

            for k = 1:length(opt.zi)
                if isfield(PathCoord.(layers{k}),pathName)
                    xpoly = PathCoord.(layers{k}).(pathName).DrawCoord(1,:)*km2m;
                    ypoly = PathCoord.(layers{k}).(pathName).DrawCoord(2,:)*km2m;
                    zpoly = -opt.zi(k)*km2m;

                    dgz_ = zeros(size(opt.gravity.survey_x));
                    for kSurv= 1:numel(dgzl)
                        x = xpoly- opt.gravity.survey_x(kSurv)*km2m;
                        y = ypoly- opt.gravity.survey_y(kSurv)*km2m;
                        z = zpoly- (-opt.gravity.survey_z(kSurv))*km2m;
                        dgz_(kSurv)   = polygrav_lamina(x,y,z);       
                    end                       
                    if ~isreal(dgz_)
                        warning('dgz_ is complex?!')
                        dgz_ = real(dgz_);
                    end
                    dgzl  = dgzl+ dgz_ *dz_lamina(k)*opt.gravity.drho(kvol);                 
                end              
            end                          
            dgz{count} = dgzl* gamma * si2mGal;           
            count=count+1;
        end
        
        % total anomaly       
        dgz{count} = zeros(size(opt.gravity.survey_x));       
        for kvol = 1:length(opt.pathNames)
            dgz{count} = dgz{count} + dgz{kvol};
        end
        
    case 'Volumes'
        
        % cell array to store anomalies
        dgz   = cell(1,length(opt.pathNames)+1);
        count = 1;

        % loop over pathnames
        for kvol = 1:length(opt.pathNames)           
            
            % current object
            pathName = opt.pathNames{kvol};           
            
            % initialize survey
            dgzl = zeros(size(opt.gravity.survey_x));

            if isfield(Volumes,pathName)
            
                % extract current object polygons
                NPoly = length(Volumes.(pathName).Polygons);
                zi    = zeros(1,NPoly);
                for kfield = 1:NPoly
                    zcurrent = unique(Volumes.(pathName).Polygons{kfield}(:,3));
                    zi(kfield) = mean(zcurrent);
                    if std(zcurrent)>1e-6
                        warning('Your volume discretisation may contain errors')
                    end
                end
                zi = zi * km2m;
                
                % get rid of numerical noise (relative level)
                round_acc = round(log10(0.00001*(max(zi)-min(zi))));
                [zi,~,zi_id] = unique(round(zi,-round_acc),'stable');

                % Thickness of lamina
                dz = diff(zi);
                dz_lamina = abs([dz(1)/2 dz(1:end-1)/2+dz(2:end)/2 dz(end)/2]);

                for k = 1:NPoly
                    xpoly = Volumes.(pathName).Polygons{k}(:,1)*km2m;
                    ypoly = Volumes.(pathName).Polygons{k}(:,2)*km2m;
                    zpoly = zi(zi_id(k));  
                    
                    % polygon orientation
                    edge = (xpoly(2:end)-xpoly(1:end-1)) .* (ypoly(2:end)+ypoly(1:end-1));
                    if sum(edge) < 0 % counter-clockwise?
                        xpoly=flip(xpoly);
                        ypoly=flip(ypoly);
                    end

                    dgz_ = zeros(size(opt.gravity.survey_x));
                    for kSurv= 1:numel(dgzl)
                        x = xpoly- opt.gravity.survey_x(kSurv)*km2m;
                        y = ypoly- opt.gravity.survey_y(kSurv)*km2m;
                        z = abs(zpoly- opt.gravity.survey_z(kSurv)*km2m);
                        dgz_(kSurv)   = polygrav_lamina(x,y,z);
                    end

                    dgzl  = dgzl+ dgz_ *dz_lamina(zi_id(k))*opt.gravity.drho(kvol);
                end
                dgz{count} = dgzl* gamma * si2mGal;           
                count=count+1;
            end
        end
        
        % total anomaly       
        dgz{count} = zeros(size(opt.gravity.survey_x));       
        for kvol = 1:length(opt.pathNames)
            dgz{count} = dgz{count} + dgz{kvol};
        end
end


end



function Vpoly = polygrav_lamina(x,y,z)
    ri     = (x(1:end-1).^2+y(1:end-1).^2).^0.5;
    rip1   = (x(2:end).^2+y(2:end).^2).^0.5;
    ri_ip1 = ( (x(1:end-1)-x(2:end)).^2 + (y(1:end-1)-y(2:end)).^2 ).^0.5;
    mi     = (y(1:end-1)./ri) .* (x(2:end)./rip1) - (y(2:end)./rip1) .* (x(1:end-1)./ri);
    fi     = (x(1:end-1)-x(2:end)) ./ ri_ip1 .* x(2:end)./rip1 + (y(1:end-1)-y(2:end)) ./ ri_ip1 .* y(2:end)./rip1;
    qi     = (x(1:end-1)-x(2:end)) ./ ri_ip1 .* x(1:end-1)./ri + (y(1:end-1)-y(2:end)) ./ ri_ip1 .* y(1:end-1)./ri;
    pi_     = (y(1:end-1)-y(2:end)) ./ ri_ip1 .* x(1:end-1) - (x(1:end-1)-x(2:end)) ./ ri_ip1 .* y(1:end-1);

    S = -ones(size(pi_));
    S(pi_>0)=1;
    W = -ones(size(mi));
    W(mi>0)=1;
    
    arg1 = x(1:end-1)./ri.*x(2:end)./rip1 + y(1:end-1)./ri.*y(2:end)./rip1;
    if (arg1(arg1>1)-1) > 1e-6
        warning ('There might be numerical problems with this geometry')
    elseif (arg1(arg1>1)-1) < 1e-6
            arg1(arg1>1) = 1; 
    end
        
    arg2 = z.*qi.*S./(pi_.^2+z.^2).^0.5;
    arg3 = z.*fi.*S./(pi_.^2+z.^2).^0.5;
    Vpoly  = ( W .* acos( arg1 ) ...
            - asin(arg2) ...
            + asin(arg3)  );
    Vpoly = sum(Vpoly);        
end
