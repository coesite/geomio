function gfwd = plotgravSVG(PathCoord,x0,z0,xobs,gobs,opt, varargin)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   plotSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
%
% x0 = [-400:400];
% z0 = zeros(size(x0));
% plotgravSVG(PathCoord,x0,z0,[],[],opt);
%
% =========================================================================

figure('name','gravity anomaly')
% vertical cross section
subplot(212)
    plotSVG(PathCoord, opt);
    xlabel('x in m')
    ylabel('z in m')
    xbnds=[get(gca,'xlim')];
    xlim(xbnds)
    box on

% anomaly signal
subplot(211)
    gfwd = plotSignals(PathCoord,opt,x0,z0);
    ylabel('Gravity anomaly in mGal')  
    box on
    xlim(xbnds);
    %set(gca,'xticklabel',[]);
    
    % plot observed anomaly if available
    if ~isempty(gobs)
        plot(xobs,gobs,'--','Color','r','LineWidth',2)
        legend([opt.pathNames,'forward model','observed'])
    else
        legend([opt.pathNames,'forward model'])
    end
    


end


function gfwd = plotSignals(PathCoord,opt,x0,z0)

layers = fieldnames(PathCoord);
if length(layers)~=1
    error('This is a 2D problem; Please provide PathCoord with only a single layer');
end

fields=PathCoord.(layers{1});
found=0;

for kvol = 1:length(opt.pathNames)
    pathName = opt.pathNames{kvol};

               if isfield(PathCoord.(layers{1}),pathName)
                    xpoly = PathCoord.(layers{1}).(pathName).DrawCoord(1,:);
                    zpoly = PathCoord.(layers{1}).(pathName).DrawCoord(2,:);    
                    Style = PathCoord.(layers{1}).(pathName).PathStyle;
                    if Style.stroke_width == 0 || isnan(Style.stroke_width)
                       Style.stroke_width = 1; 
                    end 
                    gfwd{kvol} = polygrav2D(x0,z0,xpoly,zpoly,opt.gravity.drho(kvol))';
                    plot(x0,gfwd{kvol},'Color',Style.stroke,'LineWidth',Style.stroke_width/2)
                    hold on
                    found=found+1;
               end              
end

       

if found ==1 
    
    % Total signal
    gfwd = gfwd{:}';
    
    % plot total signal
    plot(x0,gfwd,'--','Color','k','LineWidth',2)
    
elseif found > 1
    % Total signal
    gfwd{kvol+1}= sum([gfwd{:}]');

    % plot total signal
    plot(x0,gfwd{kvol+1},'--','Color','k','LineWidth',2)
else
    gfwd = [];
end
    
    
end

