function [X,Y] = extractPathsFromLayers(PathCoord,pathLabel,type)
%
%
%  [X,Z] = ExtractPathsFromLayers(PathCoord,pathLabel,'InputCoord')
%  [X,Z] = ExtractPathsFromLayers(PathCoord,pathLabel,'DrawCoord')
%  [X,Z] = ExtractPathsFromLayers(PathCoord,'LAB','DrawCoord')
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   extractPathsFromLayers.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% layerFields = fieldnames(PathCoord);



[ layerFields ] = findPathsInLayers( PathCoord, pathLabel );
noLayers    = length(layerFields);
% for iL = 1:noLayers
%     pathFields = fieldnames(PathCoord.(layerFields{1}))
% end

if isempty(layerFields)
    error(sprintf('the pathLabel "%s" was not found in the svg file',pathLabel))
end


Nic = length(PathCoord.(layerFields{1}).(pathLabel).(type));

X = zeros(Nic,noLayers);
Y = zeros(Nic,noLayers);

for il = 1:noLayers
    try
    Coord = PathCoord.(layerFields{il}).(pathLabel).(type);
    
%     if opt.closedContours
%         if ~(sum(roundn(Coord(end,:),-2) == roundn(Coord(1,:),-2)) == 2 )
%             warning(['Path "' pathLabel '" on layer "' layerFields{il} '" contains ' num2str(size(Coord,1)) 'markers and is not closed  >> close manually'])
%             Coord = [Coord; Coord(1,:)];
%         end
%     end

    X(:,il) = Coord(1,:);
    Y(:,il) = Coord(2,:);
    catch ERROR
        if strcmp(ERROR.identifier,'MATLAB:subsassigndimmismatch')
            error(['Path "' pathLabel '" on layer "' layerFields{il} '" contains ' num2str(size(Coord,2)) ' nodes, whereas it is expected to contain ' num2str(Nic) ' nodes' ])
            rethrow(ERROR)

        else
            rethrow(ERROR)
        end
    end
end



end